# M123 Repository

[TOC]

## Auftrag 1 

### DHCP in a Nutshell
Ich habe die drei Laptops mit dem Switch verbunden und den habe ich mit dem DHCP Server verbunden. Danach habe ich den DHCP Server aktiviert und den beiden Laptops eine IP Adresse vergeben. 


![DHCP bild](./Images/Screenshot%202023-12-12%20003107.png)

Hier ist ein Bild zum Datenaustausch zwischen den Geräten. 
![DHCP Bild](./Images/MicrosoftTeams-image.png)

## Auftrag 2

### DHCP mit Cisco Packet Tracer

#### Router-Konfiguration auslesen:

Ich bin zuerst in Cicso Packetracer rein, um den Auftrag zu lösen. Ich habe zuerst alle Verbindungen der Pcs überprüft. Weil diese stimmten, ging ich auf den Router und habe die Befehle ausprobiert. Da ich im enable Mode war ging ich auf Exit. Danach habe ich die folgenden Befehle eingetragen:
-show ip dhcp pool <br>
-shop ip dhcp binding <br>
-show running-config <br>

![DHCP Bild](./Images/Screenshot%202024-01-08%20130929.png)
![DHCP Bild](./Images/Screenshot%202024-01-08%20131009.png)

#### Fragen:

-Für welches Subnetz ist der DHCP Server aktiv?<br>
ip dhcp pool 1<br>
network 192.168.28.0 255.255.255.0<br>
default-router 192.168.28.1<br>

-Welche IP-Adressen ist vergeben und an welche MAC-Adressen? (Antwort als Tabelle)

|192.168.28.23 |192.168.28.24 |192.168.28.25 |192.168.28.26 |192.168.28.27 |192.168.28.28 |
|--------------|--------------|--------------|--------------|--------------|--------------|
|00D0.BC52.B29B|0001.632C.3508|0009.7CA6.4CE3|0050.0F4E.1D82|0007.ECB6.4534|00E0.8F4E.65AA|

-In welchem Range vergibt der DHCP-Server IPv4 Adressen?<br>
IP address range:<br>
 192.168.28.1- 192.168.28.254<br>

-Was hat die Konfiguration ip dhcp excluded-address zur Folge?<br>
ip dhcp excluded-address 192.168.28.1 192.168.28.22<br>
ip dhcp excluded-address 192.168.28.147 192.168.28.230<br>

-Wie viele IPv4-Adressen kann der DHCP-Server dynamisch vergeben?<br>
252 Adressen

